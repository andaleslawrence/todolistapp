import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Header from './components/header';
import AddTodo from './components/addTodo';

const App = () => {
  return (
    <SafeAreaView style={{ flex: 1, }}>
      <Header />
      <View style={styles.container}>
        <AddTodo />
      </View>


    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: 'pink',
  }
});

export default App;
