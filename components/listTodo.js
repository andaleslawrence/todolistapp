import React, { useState, useEffect } from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FlatList, StyleSheet, Text, View, TouchableOpacity, TextInput, Alert } from 'react-native';
import Modal from "react-native-modal";

const Item = ({ taskId, tasks, setTasks, title, isDone }) => {

    const [showModal, setShowModal] = useState(false)
    const [newTitle, setNewTitle] = useState('')


    return (

        <View style={{ borderBottomWidth: 1, justifyContent: 'center', padding: 15, flexDirection: 'row' }}>

            {/* STATUS */}
            <TouchableOpacity onPress={() => {
                const found = tasks.findIndex(e => e.id == taskId)
                let newArr = [...tasks]; // copying the old datas array
                newArr[found].isDone = !isDone;
                // replace e.target.value with whatever you want to change it to
                setTasks(newArr)
            }}>
                {
                    isDone ? <Ionicons name="checkbox" size={30} color="green" /> :
                        <Ionicons name="checkbox-outline" size={30} color="black" />

                }
            </TouchableOpacity>

            {/* TO-DO    */}
            <TouchableOpacity
                onPress={() => {
                    setNewTitle(title)
                    setShowModal(true)

                }}
                style={{ flex: 1, marginHorizontal: 5 }}>
                <Text style={{ fontSize: 20 }}>{title}</Text>

            </TouchableOpacity>


            {/* DELETE TO-DO */}
            <TouchableOpacity onPress={() => {
                var filtered = tasks.filter(function (value, index, arr) {
                    return value.id != taskId;
                });
                console.log('delete', filtered)
                setTasks(filtered)
            }}>
                <MaterialIcons name="delete" size={30} color="#E94560" />
            </TouchableOpacity>

            {/* MODAL */}

            <Modal
                isVisible={showModal}
                backdropColor='black'
            >

                <View style={{ backgroundColor: '#E94560', height: 150, justifyContent: 'center', alignItems: 'center', padding: 5, borderRadius: 15 }}>

                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#fff' }}>Update To-Do</Text>
                    <View style={{ borderWidth: 1, marginVertical: 10, borderColor: '#fff', width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ fontSize: 20, flex: 1, backgroundColor: 'pink' }}
                            placeholder='Enter to do . . .'
                            value={newTitle}
                            onChangeText={(text) => {
                                setNewTitle(text)
                            }}
                        />
                        <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => {
                            if (newTitle.length > 2) {
                                const found = tasks.findIndex(e => e.id == taskId)
                                let newArr = [...tasks]; // copying the old datas array
                                newArr[found].title = newTitle;
                                // replace e.target.value with whatever you want to change it to
                                setTasks(newArr)
                                setShowModal(false)
                                setNewTitle('')
                            } else {
                                Alert.alert('Warning', 'Length must be over 3 characters long.', [{ text: 'Ok', onPress: () => console.log('closed') }])
                            }

                        }}>
                            <MaterialIcons name="check" size={30} color="#fff" />
                        </TouchableOpacity>
                    </View>

                </View>
            </Modal>



        </View>
    );
}

const ListTodo = ({ tasks, setTasks, toDisplay }) => {



    const renderItem = ({ item }) => (

        <Item title={item.title} taskId={item.id} setTasks={setTasks} tasks={tasks} isDone={item.isDone} />
    );

    return (
        <View style={styles.container}>
            <FlatList
                data={toDisplay}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />

        </View>


    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        marginTop: 15,
        borderWidth: 1,
        padding: 15,
        justifyContent: 'center',
    },
});

export default ListTodo;
