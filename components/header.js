import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';

const Header = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>To-Do List</Text>
        </View>


    );
};

const styles = StyleSheet.create({
    container: {
        height: 80,
        backgroundColor: '#E94560',
        justifyContent: 'center'
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        marginLeft: 15,
        color: 'white'
    }
});

export default Header;
