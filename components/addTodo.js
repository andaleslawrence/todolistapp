import React, { useState, useEffect } from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Alert, TextInput, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import ListTodo from './listTodo';
import { Picker } from '@react-native-picker/picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AddTodo = () => {

    const [tasks, setTasks] = useState([]);
    const [taskName, setTaskname] = useState("");
    const [selectedFilter, setSelectedFilter] = useState("all");
    const [toDisplay, setToDisplay] = useState([]);

    useEffect(() => {
        if (selectedFilter == 'all') {
            setToDisplay([...tasks])
        }
        else if (selectedFilter == 'done') {
            var filtered = tasks.filter(function (value, index, arr) {
                return value.isDone == true;
            });
            setToDisplay(filtered)
        }
        else {
            var filtered = tasks.filter(function (value, index, arr) {
                return value.isDone == false;
            });
            setToDisplay(filtered)
        }
    }, [selectedFilter])

    useEffect(() => {
        const storedTasks = async () => {
            const storedData = await getData()
            console.log('data', storedData)
            setTasks(storedData)
        }
        storedTasks()

    }, [])

    useEffect(() => {
        addTasks(tasks)
        setToDisplay(tasks)
        setSelectedFilter('all')
    }, [tasks])

    //storing data
    const addTasks = async (value) => {
        try {
            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('tasks', jsonValue)
        } catch (e) {
            // saving error
        }
    }

    //reading data
    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('tasks')
            return jsonValue != null ? JSON.parse(jsonValue) : [];
        } catch (e) {
            // error reading value
        }
    }

    return (

        <View style={styles.container}>
            <View style={styles.input}>
                <TextInput
                    style={{ flex: 1, borderWidth: 1, fontSize: 20, padding: 15, borderColor: '#fff' }}
                    placeholder='Enter to do . . .'
                    value={taskName}
                    onChangeText={(text) => {
                        setTaskname(text)
                    }}
                />
                <View style={{ borderWidth: 1, width: '20%', alignItems: 'center', backgroundColor: '#E94560', borderColor: '#fff' }}>
                    <TouchableOpacity onPress={() => {
                        console.log('length', taskName.length)
                        if (taskName.length > 2) {
                            setTasks([...tasks, { id: Math.random(), title: taskName, isDone: false }])
                            setTaskname("")
                        } else {
                            Alert.alert('Warning', 'Length must be over 3 characters long.', [{ text: 'Ok', onPress: () => console.log('closed') }])
                        }

                    }}>
                        <Ionicons name="add-circle" size={60} color="#fff" />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.filter}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 5 }}>Filter by:</Text>
                <View style={{ borderWidth: 1, flex: 1, }}>
                    <Picker
                        selectedValue={selectedFilter}
                        onValueChange={(itemValue, itemIndex) =>
                            setSelectedFilter(itemValue)
                        }>
                        <Picker.Item label="All" value="all" />
                        <Picker.Item label="Done" value="done" />
                        <Picker.Item label="Not Done" value="notDone" />
                    </Picker>
                </View>
            </View>
            <View style={{ flex: 1 }}>
                <ListTodo tasks={tasks} setTasks={setTasks} toDisplay={toDisplay} />
            </View>


        </View>


    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F1BBD5',
        padding: 15,
        borderRadius: 20,
        flex: 1,
        shadowRadius: 2
    },
    input: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#fff'
    },
    filter: {
        marginTop: 15,
        paddingRight: 50,
        paddingLeft: 5,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
});

export default AddTodo;
